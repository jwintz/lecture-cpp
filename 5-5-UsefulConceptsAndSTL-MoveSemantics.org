#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 5 - Useful concepts and STL - 5.5 - Move semantics

** Motivation: eliminate unnecessary deep copies

In many situations, unnecessary deep copies are made.

In the example below, during the exchange between the two instances of
the =Text= class, we have to make 3 memory deallocations, 3 allocations,
3 character copy loops... where 3 pointer copies would be sufficient.

#+BEGIN_SRC C++
#include <cstring>
#include <iostream>

class Text
{
public:
    // For next section - don't bother yet
    friend void swap(Text& lhs, Text& rhs);

    Text(const char* string);

    // Copy constructor.
    Text(const Text& t);

    // Recopy operator; defined here due to an issue of Xeus-cling with operators
    Text& operator=(const Text& t)
    {
        std::cout << "Operator= called" << std::endl;
        if (this == &t)
            return *this ; // standard idiom to deal with auto-recopy

        delete [] data_;
        size_ = t.size_ ;
        data_ = new char[t.size_] ;
        std::copy(t.data_, t.data_ + size_, data_);

        return *this ;
    }

    ~Text();

    // Overload of operator<<, defined here due to an issue of Xeus-cling with operators.
    friend std::ostream & operator<<(std::ostream& stream, const Text& t)
    {
        return stream << t.data_ ;
    }

private:
    unsigned int size_{0};
    char* data_ = nullptr;
} ;
#+END_SRC

#+BEGIN_SRC C++
Text::Text(const char* string)
{
    std::cout << "Constructor called" << std::endl;
    size_ = std::strlen(string) + 1;
    data_ = new char[size_] ;
    std::copy(string, string + size_, data_);
}
#+END_SRC

#+BEGIN_SRC C++
Text::Text(const Text& t)
    : size_(t.size_), data_(new char [t.size_])
{
    std::cout << "Copy constructor called" << std::endl;
    std::copy(t.data_, t.data_ + size_, data_);
}
#+END_SRC

#+BEGIN_SRC C++
Text::~Text()
{
    std::cout << "Destructor called" << std::endl;
    delete[] data_;
}
#+END_SRC

#+BEGIN_SRC C++
{
    Text t1("world!") ;
    Text t2("Hello") ;

    // Swap of values:
    Text tmp = t1 ;
    t1 = t2 ;
    t2 = tmp ;

    std::cout << t1 << " " << t2 << std::endl;
}
#+END_SRC

** A traditional answer: to allow the exchange of internal data

By allowing two texts to exchange (swap) their internal data, we can
rewrite our program in a much more economical way in terms of execution
time:

#+BEGIN_SRC C++
void swap(Text& lhs, Text& rhs)
{
    unsigned int tmp_size = lhs.size_;
    char* tmp_data  = lhs.data_;
    lhs.size_ = rhs.size_;
    lhs.data_ = rhs.data_;

    rhs.size_ = tmp_size;
    rhs.data_ = tmp_data;
}
#+END_SRC

#+BEGIN_SRC C++
{
    Text t1("world!") ;
    Text t2("Hello") ;

    // Swap of values:
    swap(t1, t2);

    std::cout << t1 << " " << t2 << std::endl;
}
#+END_SRC

There is even a =std::swap= in the STL that may be overloaded for your
own types.

Now let's see how C++11 introduces new concepts to solve this (and many
other) problems in a more elegant way.

** Reminder on references in C++03

C++ references allow you to attach a new name to an existing object in
the stack or heap. All accesses and modifications made through the
reference affect the original object:

#+BEGIN_SRC C++
#include <iostream>

{
    int var = 42;
    int& ref = var; // Create a reference to var
    ref = 99;
    std::cout << "And now var is also 99: " << var << std::endl;
}
#+END_SRC

A reference can only be attached to a stable value (left value or
*l-value*), which may broadly be summarized as a value which address may
be taken (see \cite{Meyers2015} on this topic - its reading is
especially interesting concerning this topic that is not always
explained properly elsewhere - especially on the Web).

By opposition a *r-value* is a temporary value such as a literal
expression or a temporary object created by implicit conversion.

#+BEGIN_SRC C++
{
    int& i = 42 ; // Compilation error: 42 is a r-value!
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

void print(std::string& lvalue)
{
    std::cout << "l-value is " << lvalue << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    print("hello") ; // Compilation error: "hello" is a r-value!
}
#+END_SRC

Look carefully at the error message: the issue is not between
=const char[6]= and =std::string= (implicit conversion from =char*= to
=std::string= exists) but due to the reference; same function with
pass-by-copy works seemlessly:

#+BEGIN_SRC C++
#include <iostream>
#include <string>

void print_by_copy(std::string value) // no reference here!
{
    std::cout << "l- or r- value is " << value << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    print_by_copy("hello") ; // Ok!
}
#+END_SRC

Noteworthy exception: a "constant" reference (language abuse designating
a reference to a constant value) can be attached to a temporary value,
in particular to facilitate implicit conversions:

#+BEGIN_SRC C++
void print_by_const_ref(const std::string& lvalue)
{
    std::cout << "l-value is " << lvalue << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    print_by_const_ref("hello") ; // Ok!
}
#+END_SRC

** C++11/14 : temporary references

To go further, C++11 introduces the concept of *r-value reference*,
which can only refer to temporary values, and is declared using an =&&=.

#+BEGIN_SRC C++
{
    int&& i = 42;
}
#+END_SRC

#+BEGIN_SRC C++
{
    int j = 42;
    int&& k = j; // Won’t compile: j is a l-value!
}
#+END_SRC

It is now possible to overload a function to differentiate the treatment
to be performed according to whether it is provided with a stable value
or a temporary value. Below, function =f= is provided in three variants:

#+BEGIN_EXAMPLE
  void f(T&); // I : argument must be a l-value
  void f(const T&) ; // II : argument may be l-value or r-value but can't be modified
  void f(T&&); // III : argument must be a r-value
#+END_EXAMPLE

In case of a call of =f= with a temporary value, it is now form III that
will be invoked, if it is defined. This is the cornerstone of the notion
of *move semantic*.

** Function with r-value arguments

When we know that a value is temporary, we must be able to use it again,
or "loot" its content without harmful consequences; /move/ it instead of
/copying/ it. When handling large dynamic data structures, it can save
many costly operations.

Let's take a function that receives a vector of integers and replicates
it to modify it. The old way would be as follows:

#+BEGIN_SRC C++
#include <iostream>
#include <vector>

void print_double(const std::vector<int>& vec)
{
    std::cout << "print_double for l-value" << std::endl;
    std::vector<int> copy(vec);

    for (auto& item : copy)
        item *= 2;

    for (auto item : copy)
        std::cout << item << "\t";

    std::cout << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    std::vector<int> primes { 2, 3, 5, 7, 11, 13, 17, 19 };
    print_double(primes);
}
#+END_SRC

If the original object is temporary, copying it is not necessary. This
can be exploited through this overload of the function:

#+BEGIN_SRC C++
#include <iostream>

void print_double(std::vector<int>&& vec)
{
    std::cout << "print_double for r-value" << std::endl;

    for (auto& item : vec)
        item *= 2;

    for (auto item : vec)
        std::cout << item << "\t";

    std::cout << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    print_double(std::vector<int>{ 2, 3, 5, 7, 11, 13, 17, 19 });
}
#+END_SRC

** =std::move=

Now, if we get a l-value and know we do not need it anymore in the
current scope, we may choose to cast is as a r-value through a
*static_cast*:

#+BEGIN_SRC C++
{
    std::vector<int> primes { 2, 3, 5, 7, 11, 13, 17, 19 };
    print_double(static_cast<std::vector<int>&&>(primes));
}
#+END_SRC

And we see overload call is properly the one for r-values.

The syntax is a bit heavy to type, so a shorter one was introduced as
well: *=std::move=*:

#+BEGIN_SRC C++
{
    std::vector<int> primes { 2, 3, 5, 7, 11, 13, 17, 19 };
    print_double(std::move(primes)); // strictly equivalent to the static_cast in former cell!
}
#+END_SRC

Please notice that the call to =std::move= does not move =primes= per
se. It only makes it a temporary value in the eyes of the compiler, so
it is a "possibly" movable object if the context allows it; if for
instance the object doesn't define a move constructor (see next
section), no move will occur!

** Return value optimization (RVO) and copy elision

When you define a function which returns a (possibly large) object, you
might be worried unneeded copy is performed:

#+BEGIN_SRC C++
#include <vector>

std::vector<unsigned int> FiveDigitsOfPi()
{
    std::vector<unsigned int> ret { 3, 1, 4, 1, 5 };

    return ret; // copy should be incurred here... Right? (No in fact!)
}
#+END_SRC

and attempt to circumvent it by a =std::move=:

#+BEGIN_SRC C++
#include <vector>

std::vector<unsigned int> FiveDigitsOfPi_WithMove()
{
    std::vector<unsigned int> ret { 3, 1, 4, 1, 5 };

    return std::move(ret); // Don't do that!
}
#+END_SRC

or even to avoid entirely returning a large object by using a reference:

#+BEGIN_SRC C++
#include <vector>

void FiveDigitsOfPi(std::vector<unsigned int>& result)
{
    result = { 3, 1, 4, 1, 5 };
}
#+END_SRC

The second version works as you intend, but it way clunkier to use: do
you prefer:

#+BEGIN_SRC C++
{
    auto digits = FiveDigitsOfPi();
}
#+END_SRC

or:

#+BEGIN_SRC C++
{
    std::vector<unsigned int> digits;
    FiveDigitsOfPi(digits);
}
#+END_SRC

In fact, you shouldn't worry: all modern compilers provide a *return
value optimization* which guarantees never to copy the potentially large
object created.

However, it does work only when the object is returned by value, so
casting it as a rvalue reference with =std::move(ret)= actually prevents
this optimization to kick up!

So to put in a nutshell, you should (almost) never use =std::move= on a
return line (you may learn more about it in
[[https://stackoverflow.com/questions/12953127/what-are-copy-elision-and-return-value-optimization][this
StackOverflow question]]).

The only exception is detailed in item 25 of \cite{Meyers2015} and is
very specific: it is when you want to return a value that was passed by
an rvalue argument, e.g.:

#+BEGIN_SRC C++
class Matrix; // forward declaration - don't bother yet!

Matrix Add(Matrix&& lhs, const Matrix& rhs)
{
    lhs += rhs;
    return std::move(lhs); // ok in this case!
}
#+END_SRC

This case is very limited (never needed it myself so far) so I invite
you to read the item in Scott Meyer's book in you want to learn more
(items 23 to 30 are really enlightening about move semantics - very
recommended reading!).

** Move constructors

In classes, C++ introduced with move semantics two additional elements
in the canonical form of the class:

- A *move constructor*
- A *move assignment operator*

#+BEGIN_SRC C++
#include <cstring>
#include <iostream>

class Text2
{
public:
    friend void swap(Text2& lhs, Text2& rhs);

    Text2(const char* string);

    // Copy constructor.
    Text2(const Text2& t);

    // Move constructor
    Text2(Text2&& t);

    // Recopy operator; defined here due to an issue of Xeus-cling with operators
    Text2& operator=(const Text2& t)
    {
        std::cout << "Operator= called" << std::endl;
        if (this == &t)
            return *this ; // standard idiom to deal with auto-recopy

        delete [] data_;
        size_ = t.size_ ;
        data_ = new char[t.size_] ;
        std::copy(t.data_, t.data_ + size_, data_);

        return *this ;
    }

    // Move assignment operator; defined here due to an issue of Xeus-cling with operators
    Text2& operator=(Text2&& t)
    {
        std::cout << "Operator= called for r-value" << std::endl;
        if (this == &t)
            return *this;

        delete[] data_;
        size_ = t.size_;
        data_ = t.data_;

        // Don't forget to properly invalidate `t` content:
        t.size_ = 0 ;
        t.data_ = nullptr ;
        return *this ;
    }

    ~Text2();

    // Overload of operator<<, defined here due to an issue of Xeus-cling with operators.
    friend std::ostream & operator<<(std::ostream& stream, const Text2& t)
    {
        return stream << t.data_ ;
    }

private:
        unsigned int size_{0};
        char* data_ = nullptr;
} ;
#+END_SRC

#+BEGIN_SRC C++
Text2::Text2(const char* string)
{
    std::cout << "Constructor called" << std::endl;
    size_ = std::strlen(string) + 1;
    data_ = new char[size_] ;
    std::copy(string, string + size_, data_);
}
#+END_SRC

#+BEGIN_SRC C++
Text2::Text2(const Text2& t)
    : size_(t.size_), data_(new char [t.size_])
{
    std::cout << "Copy constructor called" << std::endl;
    std::copy(t.data_, t.data_ + size_, data_);
}
#+END_SRC

#+BEGIN_SRC C++
Text2::Text2(Text2&& t)
    : size_(t.size_), data_(t.data_)
{
    std::cout << "Move constructor called" << std::endl;
    t.size_ = 0 ;
    t.data_ = nullptr ;
}
#+END_SRC

#+BEGIN_SRC C++
Text2::~Text2()
{
    std::cout << "Destructor called" << std::endl;
    delete[] data_;
}
#+END_SRC

#+BEGIN_SRC C++
{
    Text2 t1("world!") ;
    Text2 t2("Hello") ;

    // Swap of values:
    Text2 tmp = std::move(t1);
    t1 = std::move(t2);
    t2 = std::move(tmp);

    std::cout << t1 << " " << t2 << std::endl;
}
#+END_SRC

With all this move semantics, the operations above are comparable to
what we achieved with the =swap= function for =Text= earlier... with the
additional benefit that this semantic is not only used for swapping two
values.

As already mentioned
[[../3-Operators/4-CanonicalForm.ipynb#%5BAdvanced%5D-The-true-canonical-class][there]],
there are specific rules called *Rule of 0*, *Rule of 3* and *Rule of
5*, which explains which constructor(s), destructor and assigmnent
operator you ought to define for your class.

** Temporary reference argument within a function

A crucial point now: if a function receives a temporary reference
argument (which can only be attached to a temporary value), within the
function this argument is considered as l-value (we can perfectly put it
to the left of an = and reassign a new value). If the function does not
itself loot the content of the variable, and transmits it to another
function (or constructor or operator), it can only reactivate its
temporary character using a call to =std::move=.

#+BEGIN_SRC C++
#include <iostream>
#include <string>

void do_stuff(std::string&& string)
{
    std::cout << "Argument given by r-value is: " << string << std::endl;
    string = "Bye!";
    std::cout << "It was nonetheless modified as it is **inside the function** a l-value: " << string << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    do_stuff("Hello!");
}
#+END_SRC

** Move semantics in the STL

All containers in the standard library are now enhanced with move
constructors and move assignment operators.

Moreover, the move semantics is not only about improving performance. There are
classes for which it makes no sense for objects to be copyable, but where it is
necessary for them to be movable. In this case, the class has a constructor per
move, and no constructor per copy.

An object that has been "emptied" as a result of a move is no longer
supposed to be useful for anything. However if not destroyed it is still
recommended, when you implement this type of class and move, to leave
the emptied object in a "valid" state; that's why we put in our =Text2=
class the =data_= pointer to =nullptr= and the =size_= to 0. The best is
of course to ensure its destruction at short notice to avoid any mishap.

** Forwarding reference (or universal reference)

Just a quick warning (you should really read \cite{Meyers2015} for an
extensive discussion on the topic; blog
[[https://www.fluentcpp.com/2018/02/06/understanding-lvalues-rvalues-and-their-references][FluentCpp]]
provides some intel about it... and tells you as well to ready Scott
Meyer's book to learn more!): seeing =&&= doesn't automatically mean it
is a r-value reference.

There is a very specific case when: - The argument is template - The
parameter is *exactly* =T&&= (not =std::vector<T&&>= for instance)

in which the syntax stands for either case (l-value or r-value)

#+BEGIN_SRC C++
#include <iostream>
#include <string>

template<class T> void PrintUniversalRef(T&& value)
{
    std::cout << value << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    PrintUniversalRef("r-value call!"); // will call a specialisation of the template for r-value

    std::string hello("l-value call!"); // will call a specialisation of the template for l-value
    PrintUniversalRef(hello);
}
#+END_SRC

Unfortunately, C++ 11 committee didn't give a name to this specific
call; Scott Meyers first publicized it under the name *universal
reference*... and was not followed by the C++ committee that finally
chose *forwarding reference*. You may therefore find one or the other
term, but the idea behind is exactly the same.

* References

[Meyers2015] Scott Meyers, ``/Effective modern C++: 42 specific ways to
improve your use of C++11 and C++14/'', 2015.
[[http://www.worldcat.org/oclc/890021237][online]]
