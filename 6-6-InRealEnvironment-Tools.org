#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 6 - C++ in a real environment - 6.6 - External tools

** Introduction

The purpose of this section is just to namedrop briefly some facilities
related to C++ that might be useful. It's far from exhaustive - I won't
present debuggers as usually they are provided directly with your IDE
(and if you're a Vim or Emacs user you probably already familiar with
[[https://www.gnu.org/software/gdb/][gdb]]).

Some tools are redundant, but they are complementary: the more you may
be able to use the better. The constraint is time: it is not
straightforward to use some of those, and even for the user-friendly
ones there is a non neglictible set-up time. But for projects that are
gaining traction it becomes a no-brainer at some point to set-up those
properly once and for all - usually part of
[[https://gitlab.inria.fr/FormationIntegrationContinue/gitlabciintroduction][continuous
integration]] process.

** Online compilers

This [[https://arne-mertz.de/2017/05/online-compilers/][blog post]]
gives several such online compilers we mentioned several times in this
tutorial.

I have used only [[http://coliru.stacked-crooked.com/][Coliru]] which
provides an API to incorporate directly some interactive code in a Web
page (disclaimer - I have not used that feature yet) and
[[http://melpon.org/wandbox][Wandbox]] which may be useful in day-to-day
work as you may try a snippet of code with many configurations. It's
useful for instance when you intend to use a bleeding-edge feature to
see which version of compilers support it or not.

** Static analyzer tools

[[http://cppcheck.sourceforge.net/][cppcheck]] is a static analyser
program; it might really help you to find some questionable constructs
in your code. As many tools, it's not 100 % accurate and sometimes it
may raise false positives, but I nonetheless recommend it warmly.

[[https://github.com/cpplint/cpplint][cpplint]] is also worth a look, we
mentioned it for instance in [[./2-FileStructure.ipynb][this chapter]]
to track down missing includes in files.

[[https://clang.llvm.org/extra/clang-tidy/][clang-tidy]] was recently
recommanded to us by colleagues from
[[https://reseau-loops.github.io/index.html][LoOPS network]].

** Valgrind

[[file:valgrind.org/][Valgrind]] is mostly known as a leak checker, but
is really a jack-of-all-trade tool; callgrind for instance may be used
to profile your code and see where most of the computation time is
spent. Some lesser-known tools are also incredibly useful:
[[https://github.com/edf-hpc/verrou][Verrou]] for instance, developed at
EDF, is a floating-point checker which helps you figure out if at some
point some of your computations might be skewed by the way the computer
approximates the real numbers.

Unfortunately, macOS support is scarse: sometimes they plainly say it is
not up-to-date, but even when they say it works the outputs were never
satisfactory for me (always check =valgrind ls= first as they recommend
on their website...). So even if I develop mostly in macOS I always fire
up valgrind in a Linux environment.

** Address sanitizer

A [[https://github.com/google/sanitizers/wiki/AddressSanitizer][recent
"concurrent"]] (once again use both if possible!) to Valgrind, which has
the advantage of running with a much better runtime (Valgrind slows down
your program tremendously). Might be integrated in some IDEs (it is in
XCode on macOS).

** Sonarqube

[[https://www.sonarqube.org/][Sonarqube]] is a development platform
which inspects your code and helps to figure out where there are issues.
It enables integration of multiple tools such as cppcheck mentioned
earlier. If you're Inria staff, an instance was set by our
[[http://sed.bordeaux.inria.fr/][Bordeaux colleagues]] and is
[[https://sonarqube.inria.fr/][available]] for you.

For open-source projects, the company behind Sonarqube provides a
[[https://sonarcloud.io/about][freely accessible platform]].

** Tests

These are frameworks to write your tests; to actually run them you
should see what your build system provides (for instance CMake comes
with CTest which takes gracefully the management of tests in your
project).

*** Boost tests

[[https://www.boost.org/doc/libs/1_69_0/libs/test/doc/html/index.html][Boost
test]] is a widely used facility to write unit and integration tests
with your code.

It might be used as a header-only or as a compiled library; for big
projects they recommend using the latter.

*** Catch2

[[https://github.com/catchorg/Catch2][Catch2]] is a more recent test
facility which is aimed at being user-friendly.

*** Google test

This test utility is the only one we know that provide mocks;
unfortunately I can't give you first-hand feedback.

** Build system
  
Already mentioned [[./1-SetUpEnvironment.ipynb#Build-system][here]].

** Code formatters

It is useful to delegate the check on your code format to an external
tool, which may take a bit of time to configurate to suit your needs.

I can mention [[http://uncrustify.sourceforge.net/][Uncrustify]]: plenty
of options to configure, even if they're not easy to figure out.

[[https://clang.llvm.org/docs/ClangFormat.html][clang-format]] probably
provides a better trade-off power/complexity but requires LLVM clang to
be installed on your system (AppleClang won't do).

** Doxygen

[[http://www.doxygen.nl/][Doxygen]] is a software to write an automatic
documentation for your functions / classes / types / you name it.

It is rather easy to set up to easy task but may become a tad more
difficult once you want to tackle more advanced features. Nonetheless it
is the de-facto standard for C++ documentation and it's really something
you should set up quite early if you're working on a project meant to
stay for a while: it's really a hassle to spend countless hours
providing after the fact such guidance in the code. As for compilers,
you should strive to provide a documentation without any warning.

An important drawback of Doxygen is that its "compilation" of your
project is much less advanced than the one provided by your compiler:

- No parallelism in some steps.
- Everything is redone at each Doxygen call.

So expect for important projects the time to generate Doxygen
documentation to be bigger than compilation time.

I have observed Doxygen is not very efficient on macOS for sizeable
projects: time is much more important than the one observed in Alpine
Docker images...

** More...

Our colleagues at Inria Bordeaux also recommend some additional tools in
the Inria SonarQube
[[https://sonarqube.bordeaux.inria.fr/pages/documentation.html#orgd4ab5b1][documentation]].
