#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 3 - Operators - TP 10

*** EXERCICE 29: =operator<<=

Introduce an =operator<<= for class =PowerOfTwoApprox= which provides
the numerator and exponent used, e.g. =10/2^4=.

Use it in =TestDisplayPowerOfTwoApprox::Display()=.
