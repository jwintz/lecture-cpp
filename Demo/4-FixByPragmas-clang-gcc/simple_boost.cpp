// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <cstdlib>
#include <iostream>

#ifdef __clang__
#pragma clang diagnostic push // starts the code block in which rules are modified
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wold-style-cast"
#pragma clang diagnostic ignored "-Wparentheses"
#pragma clang diagnostic ignored "-Wcast-qual"
#pragma clang diagnostic ignored "-Wdeprecated"
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#pragma clang diagnostic ignored "-Wweak-vtables"
#pragma clang diagnostic ignored "-Wundef"
#pragma clang diagnostic ignored "-Wzero-as-null-pointer-constant"
#pragma clang diagnostic ignored "-Wundefined-func-template"
#pragma clang diagnostic ignored "-Wc++98-compat"
#pragma clang diagnostic ignored "-Wc++98-compat-pedantic"
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wpadded"
#pragma clang diagnostic ignored "-Wnon-virtual-dtor"

#include "boost/exception/diagnostic_information.hpp"
#include "boost/filesystem.hpp"

#pragma clang diagnostic pop // go back to normal rules

#elif !defined(__INTEL_COMPILER) and defined(__GNUG__) // __GNUG__ defined for gcc, clang and Intel compilers...

# pragma GCC diagnostic push // starts the code block in which rules are modified
# pragma GCC diagnostic ignored "-Wsign-conversion"

#include "boost/exception/diagnostic_information.hpp"
#include "boost/filesystem.hpp"

#pragma GCC diagnostic pop // go back to normal rules 
#endif

int main()
{
    try
    {
        boost::filesystem::path source_path("source.txt");
        boost::filesystem::path target_path("target.txt");

        boost::filesystem::copy_file(source_path, target_path);
    }
    catch (const boost::filesystem::filesystem_error& e)
    {
        std::cerr << "Exception with Boost filesystem: " << boost::diagnostic_information(e) << std::endl;
        return EXIT_FAILURE;
    }
    
    int a; // variable intentionally left to underline my point about warnings...
    
    std::cout << "Value is " << a << std::endl;
    
    
    return EXIT_SUCCESS;
}

//
// simple_boost.cpp ends here
