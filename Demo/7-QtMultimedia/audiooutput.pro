## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

TEMPLATE = app
TARGET = audiooutput

QT += multimedia widgets

HEADERS       = audiooutput.h

SOURCES       = audiooutput.cpp \
                main.cpp

######################################################################
### audiooutput.pro ends here
