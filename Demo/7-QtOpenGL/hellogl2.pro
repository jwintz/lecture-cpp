## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

HEADERS       = glwidget.h \
                window.h \
                mainwindow.h \
                logo.h

SOURCES       = glwidget.cpp \
                main.cpp \
                window.cpp \
                mainwindow.cpp \
                logo.cpp

QT           += widgets

######################################################################
### hellogl2.pro ends here
