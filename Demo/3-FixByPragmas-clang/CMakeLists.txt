## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

cmake_minimum_required(VERSION 3.9)

# Add a compiler flag only if it is accepted.
# This macro is usually defined once and for all in a specific file which is included in the CMakeLists.txt in which
# you need it. 
include(CheckCXXCompilerFlag)
macro(add_cxx_compiler_flag _flag)
  string(REPLACE "-" "_" _flag_var ${_flag})
  check_cxx_compiler_flag("${_flag}" CXX_COMPILER_${_flag_var}_OK)

  if(CXX_COMPILER_${_flag_var}_OK)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${_flag}")
  endif()
endmacro()

set(CMAKE_CXX_COMPILER clang++ CACHE STRING "C++ compiler")
set(CMAKE_C_COMPILER clang CACHE STRING "C compiler")
set(CMAKE_CXX_STANDARD 17 CACHE INTEGER "Version of the C++ standard to use")
set(CMAKE_CXX_EXTENSIONS OFF CACHE BOOL "If ON the GNU version of the standard is used.")

project(GettingStartedWithModernCpp)

find_package(Boost 1.36.0 COMPONENTS filesystem system REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})

add_cxx_compiler_flag("-Weverything") # all warnings for clang
add_cxx_compiler_flag("-Wall") # for gcc (and recognized by clang)
add_cxx_compiler_flag("-Wextra") # for gcc  (and recognized by clang)
add_cxx_compiler_flag("-Wsign-conversion") # a triggered warning in gcc (-Wall -Wextra alone don't show warnings)

add_executable(simple_boost simple_boost.cpp)

target_link_libraries(simple_boost Boost::filesystem Boost::system)

######################################################################
### CMakeLists.txt ends here
