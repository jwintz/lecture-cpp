## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

TEMPLATE = app

QT += widgets

HEADERS += colorswatch.h mainwindow.h toolbar.h

SOURCES += colorswatch.cpp mainwindow.cpp toolbar.cpp main.cpp

build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

RESOURCES += mainwindow.qrc

######################################################################
### mainwindow.pro ends here
