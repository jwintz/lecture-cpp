#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 2 - Object programming - TP 5

*** EXERCICE 14: transform =compute_power_of_2_approx()= into a =PowerOfTwoApprox= method

What we did in exercice 1 is a bit clunky: the object is introduced but
we keep spending time fetching the data attribute to act upon them (even
initialization to 0 is done manually several times).

We'll streamline this a bit by making some of the functions methods of
the =PowerOfTwoApprox= struct; starting with
=compute_power_of_2_approx()= that will become a method named
=Compute()=. This method will change the internal data attributes rather
than changing an output argument.

Output should remain unchanged after this modification.
