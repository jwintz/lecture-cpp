// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <cmath> // for std::round

// /////////////////////////////////////////////////////////////////////////////
//! Returns `number` * (2 ^ `exponent`)
// /////////////////////////////////////////////////////////////////////////////

int times_power_of_2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    
// /////////////////////////////////////////////////////////////////////////////
//! Round to `x` the nearest integer.
// /////////////////////////////////////////////////////////////////////////////

int round_as_int(double x)
{
    return static_cast<int>(std::round(x));
}

// /////////////////////////////////////////////////////////////////////////////
// Maximum integer that might be represented with `Nbits` bits.
// /////////////////////////////////////////////////////////////////////////////

int max_int(int Nbits)
{ 
    return (times_power_of_2(1, Nbits) - 1);
}

// /////////////////////////////////////////////////////////////////////////////
//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
// /////////////////////////////////////////////////////////////////////////////

void helper_display_power_of_2_approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = times_power_of_2(1, exponent);   
    numerator = round_as_int(value * denominator);
}

// /////////////////////////////////////////////////////////////////////////////
//! Display the approximation of the given argument that fits in the given number of bits.
// /////////////////////////////////////////////////////////////////////////////

void display_power_of_2_approx(int Nbits, double value)
{
    int max_numerator = max_int(Nbits);
    int numerator {}, denominator{}, exponent {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        helper_display_power_of_2_approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    helper_display_power_of_2_approx(value, --exponent, numerator, denominator);
    
    double approx = static_cast<double>(numerator) / denominator; 
        
    std::cout << "[With " << Nbits << " bits]: " << value << " ~ " << approx << " (" << numerator << 
        " / 2^" << exponent << ')' << std::endl;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
        
    for (int Nbits = 2; Nbits <= 8; Nbits += 2)
        display_power_of_2_approx(Nbits, 0.65) ;
    
    std::cout << std::endl;

    for (int Nbits = 2; Nbits <= 8; Nbits += 2)
        display_power_of_2_approx(Nbits, 0.35) ;

    return EXIT_SUCCESS;
}

//
// exercice5.cpp ends here
