// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <cmath> // for std::round


// /////////////////////////////////////////////////////////////////////////////
// Returns `number` * (2 ^ `exponent`)
// /////////////////////////////////////////////////////////////////////////////

int times_power_of_2(int number, int exponent)
{
    while (exponent > 0) {

        number *= 2; 
        exponent -= 1; 
    }

    while (exponent < 0) {
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    
// /////////////////////////////////////////////////////////////////////////////
//! Round to `x` the nearest integer.
// /////////////////////////////////////////////////////////////////////////////

int round_as_int(double x)
{
    return static_cast<int>(std::round(x));
}

// /////////////////////////////////////////////////////////////////////////////
//! Display the approximation of the given argument for the exponents from 1 to 8.
// /////////////////////////////////////////////////////////////////////////////

void display_power_of_2_approx(double value)
{
    for (int exponent = 1; exponent <= 8; ++exponent)
    {
        int denominator = times_power_of_2(1, exponent);        
        int numerator = round_as_int(value * denominator);
        double approx = static_cast<double>(numerator) / denominator; 
        
        std::cout << value << " ~ " << approx << " (" << numerator << 
            " / 2^" << exponent << ')' << std::endl;
    }
    
    std::cout << std::endl;
    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 

    display_power_of_2_approx(.65);
    display_power_of_2_approx(.35);
    
    return EXIT_SUCCESS;
}

//
// exercice3.cpp ends here
