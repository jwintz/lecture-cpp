// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <cmath> // for std::round

// /////////////////////////////////////////////////////////////////////////////
//! Returns `number` * (2 ^ `exponent`)
// /////////////////////////////////////////////////////////////////////////////

int times_power_of_2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    
// /////////////////////////////////////////////////////////////////////////////
//! Round to `x` the nearest integer.
// /////////////////////////////////////////////////////////////////////////////

int round_as_int(double x)
{
    return static_cast<int>(std::round(x));
}

// /////////////////////////////////////////////////////////////////////////////
//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY)
//principle! /////////////////////////////////////////////////////////////////////////////

void helper_display_power_of_2_approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = times_power_of_2(1, exponent);   
    numerator = round_as_int(value * denominator);
}

// /////////////////////////////////////////////////////////////////////////////
//! Display the approximation of the given argument that does not exceed the
// chosen maximum
//numerator. /////////////////////////////////////////////////////////////////////////////

void display_power_of_2_approx(int max_numerator, double value)
{
    int numerator {}, denominator{}, exponent {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        helper_display_power_of_2_approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    helper_display_power_of_2_approx(value, --exponent, numerator, denominator);
    
    double approx = static_cast<double>(numerator) / denominator; 
        
    std::cout << "[With numerator <= "<< max_numerator << "]: " << value << " ~ " << approx << " (" << numerator << 
        " / 2^" << exponent << ')' << std::endl;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 

    display_power_of_2_approx(15, 0.65);
    display_power_of_2_approx(255, 0.65);

    display_power_of_2_approx(15, 0.35);
    display_power_of_2_approx(255, 0.35);
    
    return EXIT_SUCCESS;
}

//
// exercice4.cpp ends here
