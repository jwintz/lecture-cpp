// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <sstream> // for std::ostringstream
#include <string>
#include <cmath> // for std::round

// /////////////////////////////////////////////////////////////////////////////
//! Returns `number` * (2 ^ `exponent`)
// /////////////////////////////////////////////////////////////////////////////

int times_power_of_2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    
// /////////////////////////////////////////////////////////////////////////////
//! Round to `x` the nearest integer.
// /////////////////////////////////////////////////////////////////////////////

int round_as_int(double x)
{
    return static_cast<int>(std::round(x));
}

// /////////////////////////////////////////////////////////////////////////////
// Maximum integer that might be represented with `Nbits` bits.
// /////////////////////////////////////////////////////////////////////////////

int max_int(int Nbits)
{ 
    return (times_power_of_2(1, Nbits) - 1);
}

// /////////////////////////////////////////////////////////////////////////////
//! Class to group the two integers used in the approximation we define.
// /////////////////////////////////////////////////////////////////////////////

class PowerOfTwoApprox
{
public:
    
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    double AsDouble() const;
    
    //! Accessor to numerator.
    int GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
   /*! 
    * \brief Multiply the approximate representation by an integer. 
    * 
    * \param[in] coefficient Integer coefficient by which the object is multiplied.
    * 
    * \return An approximate integer result of the multiplication.
    */
    int Multiply(int coefficient) const;
        
private:
        
    int numerator_ {};
    int exponent_ {};    
};  


// /////////////////////////////////////////////////////////////////////////////
//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function
// /////////////////////////////////////////////////////////////////////////////
.
void helper_compute_power_of_2_approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = times_power_of_2(1, exponent);   
    numerator = round_as_int(value * denominator);
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = max_int(Nbits);
    
    int& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        helper_compute_power_of_2_approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    helper_compute_power_of_2_approx(value, --exponent, numerator, denominator);
}


double PowerOfTwoApprox::AsDouble() const
{
    int denominator = times_power_of_2(1, exponent_);
    return static_cast<double>(numerator_) / denominator;
}


int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}


int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}


int PowerOfTwoApprox::Multiply(int coefficient) const
{
    return times_power_of_2(GetNumerator() * coefficient, -GetExponent());
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

enum class RoundToInteger { no, yes };

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay();
    
    //! Pure virtual method Do().
    virtual void Do(int Nbits) const = 0;


protected:
    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   void PrintLine(int Nbits, double exact, double approx,
                  std::string optional_string1 = "", std::string optional_string2 = "",
                  RoundToInteger do_round_to_integer = RoundToInteger::no) const;
                  
private:
    
    //! Resolution.
    const int resolution_;
  
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::Do(int Nbits) const
{
    static_cast<void>(Nbits); // neutralize warning about unused argument at no runtime cost.
}


void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            std::string optional_string1, std::string optional_string2,
                            RoundToInteger do_round_to_integer) const
{
    int error = round_as_int(resolution_ * std::fabs(exact - approx) / exact);
 
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? round_as_int(exact) : exact)  << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << resolution_ << "]" 
        << std::endl;    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox(int resolution);
    
    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Pure virtual method Do().
    virtual void Do(int Nbits) const override = 0;
    
protected:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayPowerOfTwoApprox::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }    

TestDisplayPowerOfTwoApprox::~TestDisplayPowerOfTwoApprox() = default;


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    const double approx = approximation.AsDouble();
    
    std::ostringstream oconv;
    oconv << "  (" << approximation.GetNumerator() << "/2^" << approximation.GetExponent() << ")";    
    PrintLine(Nbits, value, approx, "", oconv.str());
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox065(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox065() override;
    
     //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const override;

};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayPowerOfTwoApprox065::TestDisplayPowerOfTwoApprox065(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }


TestDisplayPowerOfTwoApprox065::~TestDisplayPowerOfTwoApprox065() = default;


void TestDisplayPowerOfTwoApprox065::Do(int Nbits) const
{
    Display(Nbits, 0.65);    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox035(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox035() override;
    
     //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const override;

};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayPowerOfTwoApprox035::TestDisplayPowerOfTwoApprox035(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }

TestDisplayPowerOfTwoApprox035::~TestDisplayPowerOfTwoApprox035() = default;


void TestDisplayPowerOfTwoApprox035::Do(int Nbits) const
{
    Display(Nbits, 0.35);    
}


// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayMultiply : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayMultiply(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplayMultiply() override;
    
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
    
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayMultiply::TestDisplayMultiply(int resolution)
: TestDisplay(resolution)
{ }

TestDisplayMultiply::~TestDisplayMultiply() = default;


void TestDisplayMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);
}


void TestDisplayMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    
    PowerOfTwoApprox approximation1(Nbits, value1);
    PowerOfTwoApprox approximation2(Nbits, value2);    
    
    int approx = approximation1.Multiply(coefficient1) + approximation2.Multiply(coefficient2);
    
    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 << " + " << value2 << " * " << coefficient2 << " = ";
    
    PrintLine(Nbits, exact, static_cast<double>(approx), oconv.str(), "", RoundToInteger::yes);
}


//! Function for error handling. We will see later how to fulfill the same functionality more properly.
[[noreturn]] void error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


class TestDisplayContainer
{
public:
    
    //! Constructor.
    TestDisplayContainer(std::size_t capacity);
    
    //! Destructor.
    ~TestDisplayContainer();

    //! Add a new test_display_register.
    //! At each call, the item to be registered is put at the first available position and internal current_position_
    //! is incremented. If the end-user attempts to register more than three items, the error() function is called.
    void Register(TestDisplay* test_display);
    
    //! Accessor to the i-th object in the container.
    const TestDisplay& GetElement(std::size_t i) const;
    
    //! Get the number of elements actually stored in the class (nullptr don't count).
    std::size_t GetSize() const;
    
    
private:
    
    //! Get the maximal number of elements that might be stored in the container.
    std::size_t GetCapacity() const;
    
private:    
    
    //! Maximum number of items that might be registered in the container.
    const std::size_t capacity_;
    
    //! List of all known `TestDisplay` objects.
    TestDisplay** list_;
    
    //! Index to place the next register object. If '3', no more object may be registered.
    std::size_t current_position_ {};

};


TestDisplayContainer::TestDisplayContainer(std::size_t capacity)
: capacity_(capacity)
{
    list_ = new TestDisplay*[capacity];
    
    for (auto i = 0ul; i < capacity; ++i)
        list_[i] = nullptr;
}


TestDisplayContainer::~TestDisplayContainer()
{
    for (auto i = 0ul; i < capacity_; ++i)
        delete list_[i];
    
    delete[] list_; // don't forget the [] to delete an array!
}


void TestDisplayContainer::Register(TestDisplay* test_display)
{
    if (current_position_ >= capacity_)
        error("TestDisplayContainer is already full; impossible to register a new element!");
    
    list_[current_position_] = test_display;
    ++current_position_;
}


const TestDisplay& TestDisplayContainer::GetElement(std::size_t i) const
{
    if (i >= GetCapacity())
        error("You try to access an element out of bounds!");
    
    if (list_[i] == nullptr) // equivalent to i >= GetSize()
        error("You try to access an element that was never initialized!");
    
    return *list_[i];    
}


std::size_t TestDisplayContainer::GetCapacity() const
{
    return capacity_;
}


std::size_t TestDisplayContainer::GetSize() const
{
    return current_position_;
}


//! For each container stored, loop oover all those bits and print the result on screen.
void loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container)
{
    for (auto i = 0ul; i < container.GetSize(); ++i)
    {
        decltype(auto) current_test_display = container.GetElement(i);
            
        for (int Nbits = initial_Nbit; Nbits <= final_Nbit; Nbits += increment_Nbit)
            current_test_display.Do(Nbits);
        std::cout << std::endl;
    }
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother      

    TestDisplayContainer container(3);
    
    container.Register(new TestDisplayPowerOfTwoApprox065(1000000));
    container.Register(new TestDisplayPowerOfTwoApprox035(1000000));
    container.Register(new TestDisplayMultiply(10000));
    
    loop(4, 16, 4, container);
    
    return EXIT_SUCCESS;
}


//
// exercice25.cpp ends here
