// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <cmath> // for std::round

// /////////////////////////////////////////////////////////////////////////////
//! Returns `number` * (2 ^ `exponent`)
// /////////////////////////////////////////////////////////////////////////////

int times_power_of_2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    
// /////////////////////////////////////////////////////////////////////////////
//! Round to `x` the nearest integer.
// /////////////////////////////////////////////////////////////////////////////

int round_as_int(double x)
{
    return static_cast<int>(std::round(x));
}

// /////////////////////////////////////////////////////////////////////////////
// Maximum integer that might be represented with `Nbits` bits.
// /////////////////////////////////////////////////////////////////////////////

int max_int(int Nbits)
{ 
    return (times_power_of_2(1, Nbits) - 1);
}

// /////////////////////////////////////////////////////////////////////////////
//! Class to group the two integers used in the approximation we define.
// /////////////////////////////////////////////////////////////////////////////

class PowerOfTwoApprox
{
public:
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    double AsDouble() const;
    
    //! Accessor to numerator.
    int GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
   /*! 
    * \brief Multiply the approximate representation by an integer. 
    * 
    * \param[in] coefficient Integer coefficient by which the object is multiplied.
    * 
    * \return An approximate integer result of the multiplication.
    */
    int Multiply(int coefficient) const;
        
private:
    int numerator_ {};
    int exponent_ {};    
};  


// /////////////////////////////////////////////////////////////////////////////
//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
// /////////////////////////////////////////////////////////////////////////////

void helper_compute_power_of_2_approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = times_power_of_2(1, exponent);   
    numerator = round_as_int(value * denominator);
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = max_int(Nbits);
    
    int& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        helper_compute_power_of_2_approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    helper_compute_power_of_2_approx(value, --exponent, numerator, denominator);
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

double PowerOfTwoApprox::AsDouble() const
{
    int denominator = times_power_of_2(1, exponent_);
    return static_cast<double>(numerator_) / denominator;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int PowerOfTwoApprox::Multiply(int coefficient) const
{
    return times_power_of_2(GetNumerator() * coefficient, -GetExponent());
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox
{
public:
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void TestDisplayPowerOfTwoApprox::Do(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    const double approx = approximation.AsDouble();
    
    const double error = std::fabs(value - approx) / value;
        
    std::cout << "[With " << Nbits << " bits]: " << value << " ~ " << approx << " (" << approximation.GetNumerator() << 
        " / 2^" << approximation.GetExponent() << ")  [error = " << round_as_int(100. * error) << "/100]" << std::endl;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayMultiply
{
public:
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void TestDisplayMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void TestDisplayMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    int rounded = round_as_int(exact);
    
    PowerOfTwoApprox approximation1(Nbits, value1);
    PowerOfTwoApprox approximation2(Nbits, value2);    
    
    int approx = approximation1.Multiply(coefficient1) + approximation2.Multiply(coefficient2);
    
    const double error = std::fabs(exact - approx) / exact;
    
    std::cout << "[With " << Nbits << " bits]: " << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = " 
        << rounded << " ~ " << approx 
        << "  [error = " << round_as_int(1000. * error) <<  "/1000]" <<  std::endl;    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
     
    TestDisplayPowerOfTwoApprox test_display_approx;
     
    for (int Nbits = 2; Nbits <= 8; Nbits += 2)
         test_display_approx.Do(Nbits); 

    std::cout << std::endl;

    TestDisplayMultiply test_display_multiply;

    for (int Nbits = 1; Nbits <= 8; ++Nbits)
        test_display_multiply.Do(Nbits);

    return EXIT_SUCCESS;
}

//
// exercice19.cpp ends here
