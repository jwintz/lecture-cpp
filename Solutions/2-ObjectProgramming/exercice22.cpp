// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>
#include <sstream> // for std::ostringstream
#include <string>
#include <cmath> // for std::round

// /////////////////////////////////////////////////////////////////////////////
//! Returns `number` * (2 ^ `exponent`)
// /////////////////////////////////////////////////////////////////////////////

int times_power_of_2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    
// /////////////////////////////////////////////////////////////////////////////
//! Round to `x` the nearest integer.
// /////////////////////////////////////////////////////////////////////////////

int round_as_int(double x)
{
    return static_cast<int>(std::round(x));
}

// /////////////////////////////////////////////////////////////////////////////
// Maximum integer that might be represented with `Nbits` bits.
// /////////////////////////////////////////////////////////////////////////////

int max_int(int Nbits)
{ 
    return (times_power_of_2(1, Nbits) - 1);
}

// /////////////////////////////////////////////////////////////////////////////
//! Class to group the two integers used in the approximation we define.
// /////////////////////////////////////////////////////////////////////////////

class PowerOfTwoApprox
{
public:
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    double AsDouble() const;
    
    //! Accessor to numerator.
    int GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
   /*! 
    * \brief Multiply the approximate representation by an integer. 
    * 
    * \param[in] coefficient Integer coefficient by which the object is multiplied.
    * 
    * \return An approximate integer result of the multiplication.
    */
    int Multiply(int coefficient) const;
        
private:
    int numerator_ {};
    int exponent_ {};    
};  

// /////////////////////////////////////////////////////////////////////////////
//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
// /////////////////////////////////////////////////////////////////////////////

void helper_compute_power_of_2_approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = times_power_of_2(1, exponent);   
    numerator = round_as_int(value * denominator);
}

PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = max_int(Nbits);
    
    int& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        helper_compute_power_of_2_approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    helper_compute_power_of_2_approx(value, --exponent, numerator, denominator);
}

double PowerOfTwoApprox::AsDouble() const
{
    int denominator = times_power_of_2(1, exponent_);
    return static_cast<double>(numerator_) / denominator;
}


int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}


int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}


int PowerOfTwoApprox::Multiply(int coefficient) const
{
    return times_power_of_2(GetNumerator() * coefficient, -GetExponent());
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

enum class RoundToInteger { no, yes };

// /////////////////////////////////////////////////////////////////////////////
// TestDisplay declaration
// /////////////////////////////////////////////////////////////////////////////

class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay() = 0;


protected:
    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   void PrintLine(int Nbits, double exact, double approx,
                  std::string optional_string1 = "", std::string optional_string2 = "",
                  RoundToInteger do_round_to_integer = RoundToInteger::no) const;
                  
private:
    
    //! Resolution.
    const int resolution_;
  
};

// /////////////////////////////////////////////////////////////////////////////
// TestDisplay implementation
// /////////////////////////////////////////////////////////////////////////////

TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            std::string optional_string1, std::string optional_string2,
                            RoundToInteger do_round_to_integer) const
{
    int error = round_as_int(resolution_ * std::fabs(exact - approx) / exact);
 
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? round_as_int(exact) : exact)  << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << resolution_ << "]" 
        << std::endl;    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplayPowerOfTwoApprox() ;
    
protected:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayPowerOfTwoApprox::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }    

TestDisplayPowerOfTwoApprox::~TestDisplayPowerOfTwoApprox() = default;


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    const double approx = approximation.AsDouble();
    
    std::ostringstream oconv;
    oconv << "  (" << approximation.GetNumerator() << "/2^" << approximation.GetExponent() << ")";    
    PrintLine(Nbits, value, approx, "", oconv.str());
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox065(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox065() override;
    
     //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;

};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayPowerOfTwoApprox065::TestDisplayPowerOfTwoApprox065(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }


TestDisplayPowerOfTwoApprox065::~TestDisplayPowerOfTwoApprox065() = default;


void TestDisplayPowerOfTwoApprox065::Do(int Nbits) const
{
    Display(Nbits, 0.65);    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox035(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox035() override;
    
     //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;

};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayPowerOfTwoApprox035::TestDisplayPowerOfTwoApprox035(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }

TestDisplayPowerOfTwoApprox035::~TestDisplayPowerOfTwoApprox035() = default;


void TestDisplayPowerOfTwoApprox035::Do(int Nbits) const
{
    Display(Nbits, 0.35);    
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class TestDisplayMultiply : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayMultiply(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplayMultiply() override;
    
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
    
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

TestDisplayMultiply::TestDisplayMultiply(int resolution)
: TestDisplay(resolution)
{ }

TestDisplayMultiply::~TestDisplayMultiply() = default;

void TestDisplayMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);
}

void TestDisplayMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    
    PowerOfTwoApprox approximation1(Nbits, value1);
    PowerOfTwoApprox approximation2(Nbits, value2);    
    
    int approx = approximation1.Multiply(coefficient1) + approximation2.Multiply(coefficient2);
    
    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 << " + " << value2 << " * " << coefficient2 << " = ";
    
    PrintLine(Nbits, exact, static_cast<double>(approx), oconv.str(), "", RoundToInteger::yes);
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother      

    TestDisplayPowerOfTwoApprox065 test_display_approx065(100);     

    for (int Nbits = 2; Nbits <= 8; Nbits += 2)
         test_display_approx065.Do(Nbits); 

    std::cout << std::endl;
    
    TestDisplayPowerOfTwoApprox035 test_display_approx035(100);     

    for (int Nbits = 2; Nbits <= 8; Nbits += 2)
         test_display_approx035.Do(Nbits); 
    
    std::cout << std::endl;

    TestDisplayMultiply test_display_multiply(1000);

    for (int Nbits = 1; Nbits <= 8; ++Nbits)
        test_display_multiply.Do(Nbits);

    return EXIT_SUCCESS;
}

//
// exercice22.cpp ends here
