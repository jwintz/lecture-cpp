// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <string>
#include <exception>

class Error : public std::exception
{
public:
    //! Constructor.
    Error(const std::string& message);
    
    //! Overrides what method.
    virtual const char* what() const noexcept override;
    
private:
    const std::string message_;
};

#endif

//
// Error.hpp ends here
