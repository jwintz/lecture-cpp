#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 6 - C++ in a real environment - 6.1. - Set up environment

** Introduction

I will present here briefly how to set up a minimal development
environment... only in Unix-like systems: sorry for Windows developers,
but I have never set up a Windows environment for development.

This will explain installation for two mainstreams compilers:
[[https://en.wikipedia.org/wiki/GNU_Compiler_Collection][GNU compiler
for C++]] and [[https://en.wikipedia.org/wiki/Clang][clang++]].

** Installing a compiler

*** Ubuntu / Debian

For Ubuntu I recommend using =g++=: setting up =clang++= along with its
standard library is rather dreadful...

To install g++ you may use the line:

#+BEGIN_SRC C++
// In a terminal
sudo apt-get install -y g++
#+END_SRC

However, Ubuntu is rather conservative and the version you get might be
a bit dated and it might be problematic if you intend to use the
bleeding-edged features from the latest C++ standard (even if it's now
better than what it used to be: with Ubuntu 18.04 you get the decent
g++-7.3 now).

To get a more recent version, you need to use a
[[https://launchpad.net/ubuntu/+ppas][PPA]]:

#+BEGIN_SRC C++
// In a terminal

// To enable `add-apt-repository` command; probably not required in a full-fledged installation
// but you will need it in a Docker image for instance.
sudo apt-get install --no-install-recommends -y software-properties-common

// Adding PPA and making its content available to `apt`.
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update

// Installing the more recent gcc; which is 9 at the time of this writing
sudo apt-get install -y g++-9
#+END_SRC

And to tell the system which version to use, the command is:

#+BEGIN_SRC C++
// In a terminal
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 100
#+END_SRC

More realistically, you will install gcc and perhaps gfortran as well;
the following command make sure all are kept consistent (you do not want
to mesh gcc 7 with g++ 9 for instance...):

#+BEGIN_SRC C++
// In a terminal
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 100
  --slave /usr/bin/g++ g++ /usr/bin/g++-9
  --slave /usr/bin/gfortran gfortran /usr/bin/gfortran-9
#+END_SRC

A [[./Docker/Dockerfile.ubuntu][Dockerfile]] was provided to create an
image with this basic setup.

*** Fedora

For development purposes I rather like Fedora, which provides more
recent versions than Ubuntu and also a simple clang installation.

**** g++

#+BEGIN_SRC C++
// In a terminal
sudo dnf install -y gcc gcc-c++ gcc-gfortran
#+END_SRC

**** clang++
#+BEGIN_SRC C++
// In a terminal
sudo dnf install -y clang
#+END_SRC

Dockerfiles have also been provided for
[[./Docker/Dockerfile.fedora.gcc][gcc]] and
[[./Docker/Dockerfile.fedora.clang][clang]] settings.

*** macOS

- Install XCode from the AppStore
- Install if requested the command line tools

This will install /Apple Clang/ which is a customized version of
=clang=. Unfortunately, Apple stopped providing the version of
mainstream clang it is built upon; it's therefore more difficult to
check if a new standard feature is already supported or not.

It is also possible to install gcc on macOS; I personally use the
tarballs provided by [[http://hpc.sourceforge.net/][this site]].

** Editor

You will need a
[[https://en.wikipedia.org/wiki/Source_code_editor][source code editor]]
to write your code; usually your system will provide a very basic one
and you may be able to install another on your own.

From my experience, there are essentially two types of developers:

- Those that revel in using
  [[https://en.wikipedia.org/wiki/Vim_(text_editor)][VIM]] *or*
  [[https://en.wikipedia.org/wiki/Emacs][emacs]], which are lightweight
  editors that have been around for decades and are rather powerful once
  you've climbed a (very) steep learning curve.
- Those that will like
  [[https://en.wikipedia.org/wiki/Integrated_development_environment][integrated
  development environment]] more, which provides more easily some
  facilities (that can often be configured the hard way in the
  aforementioned venerable text editors) but are more
  resource-intensive.

I suggest you take whichever you're the most comfortable with and don't
bother about the zealots that tell you their way is absolutely the best
one.

/Vim/ and /emacs/ are often either already installed or available easily
with a distribution package (/apt/, /dnf/, etc...); for IDEs here are
few of them:

- [[https://code.visualstudio.com/][Visual Studio Code]], which gained
  traction in last few years and is one of the most sizeable GitHub
  project. This is an open-source and multi-platform editor maintained
  by Microsoft, not to be confused with
  [[https://visualstudio.microsoft.com/?rr=https%3A%2F%2Fwww.google.com%2F][Visual
  Studio]] - also provided by Microsoft on Windows (and with a fee).
- [[https://www.jetbrains.com/clion/][CLion]] by JetBrains is also a
  rising star in IDEs; a free version is available for academics.
- [[https://www.eclipse.org/cdt/][Eclipse CDT]] and
  [[https://netbeans.org/][NetBeans]] are other IDEs with more mileage.
- [[https://www.qt.io/qt-features-libraries-apis-tools-and-ide][QtCreator]]
  is not just for Qt edition and might be used as a C++ IDE as well.
- [[https://developer.apple.com/xcode][XCode]] is the editor provided by
  Apple on macOS.

** Software configuration manager

A
[[https://en.wikipedia.org/wiki/Software_configuration_management][software
configuration manager]], sometimes abbreviated as *SCM*, is important
when you're writing code that is meant to stay at least a while (and
very handy even if that is not the case).

It is not useful only when you're working with someone else: if at some
point you're lost in your code and don't understand why what was working
perfectly few hours ago is now utterly broken it is really helpful to be
able to compare what has changed since this last commit.

The most obvious choice for a SCM is [[https://git-scm.com][git]] which
is now widely abroad and has become the /de facto/ standard. /git/ is
extremely versatile but you can already do a lot of version control with
around 10 commands so the learning curve is not as steep as you may
fear.

git is generally already installed on your system or readily available
through your package manager.

** Build system

Handling properly the compilation of the code is not an easy task: many
tutorial skip entirely the topic or just show a very basic example that
is very far removed from a real project with potentially many
third-party dependencies. This is understandable (and I will mostly do
the same): using properly a build system is not trivial and may be the
topic on a full lecture of its own.

The usual possibilities are:

- Build system provided by your IDE. Might be easier to use (definitely
  the case for XCode which I'm familiar with once you grasp how it is
  intended to work) but you bind your potential users to use the same
  IDE (even if now some relies upon CMake we'll see shortly).
- [[https://en.wikipedia.org/wiki/Makefile][Makefile]] is the venerable
  ancestor, which is really too painful to write and not automated
  enough for my taste.
- [[https://cmake.org][CMake]] is the build system probably with the
  more traction now; it is a cross-platform build system which is rather
  powerful but not that easy to learn. Official documentation is terse;
  you may try [[https://cliutils.gitlab.io/modern-cmake/][this]] or
  [[https://cgold.readthedocs.io/en/latest/][that]] to understand it
  better. Please notice CMake was heavily changed when switching from
  version2 to version 3; take a recent documentation if you want to
  learn "modern" CMake. The principle of CMake is to provide a generic
  configuration that may be used for different build tools: by default
  you generate a Makefile, but you may choose another generator such as
  Ninja (see below) or a specific IDE.
- [[https://mesonbuild.com/][meson]] is a more recent alternative which
  aims to be simpler to use than CMake. Never used it so can't say much
  about it.
- [[https://www.scons.org/][SCons]] is a build system built upon Python
  which lets you write your own Python functions in the build system.
  The concept is appealing, but the actual use is actually dreadful and
  the provided build is much slower than what other build system
  provides. Avoid it!
- [[https://ninja-build.org][Ninja]] is presented on this website as /a
  small build system with a focus on speed. It differs from other build
  systems in two major respects: it is designed to have its input files
  generated by a higher-level build system, and it is designed to run
  builds as fast as possible/. It is my favorite generator to use with
  CMake; meson also enables usage of Ninja under the hood.
