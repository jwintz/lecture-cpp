#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 2 - Object programming - TP 6

*** EXERCICE 15: change interface of =PowerOfTwoApprox= struct

- Transform =Compute()= into a constructor. This constructor can't any
  longer return a double value; introduce a method =AsDouble()= which
  returns the approximation.
- Assign a default value of =0= to data attributes (if not already done
  previously!)

Expected output is the same as previously.
