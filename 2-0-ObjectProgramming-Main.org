#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* Object programming

 + [[file:2-1-ObjectProgramming-Introduction.org][2.1. Introduction]]                  [[file:2-1-ObjectProgramming-TP.org][[+TP]​]]
 + [[file:2-2-ObjectProgramming-Member-functions.org][2.2. Member Functions]]              [[file:2-2-ObjectProgramming-TP.org][[+TP]​]]
 + [[file:2-3-ObjectProgramming-Constructors-destructor.org][2.3. Constructors & Destructors]]    [[file:2-3-ObjectProgramming-TP.org][[+TP]​]]
 + [[file:2-4-ObjectProgramming-Encapsulation.org][2.4. Encapsulation]]                 [[file:2-4-ObjectProgramming-TP.org][[+TP]​]]
 + [[file:2-5-ObjectProgramming-Static.org][2.5. Static]]
 + [[file:2-6-ObjectProgramming-Inheritance.org][2.6. Inheritance]]                   [[file:2-6-ObjectProgramming-TP.org][[+TP]​]]
