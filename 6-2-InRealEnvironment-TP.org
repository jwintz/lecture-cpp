#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 6 - C++ in a real environment - TP 17

*** EXERCICE 43

Reorganize the project in several files:

- Only =loop()= and =main()= functions should be put in /main.cpp/.
- =Error= should be declared in =Error.hpp= and defined in =Error.cpp=.
- Free functions such as =times_power_of_2= and =round_as_int= should be
  put in =Tools= files. As all these definitions are template, they
  should be in a header file (that might be the same as the one used for
  declaration, or a different one if you want to split declaration and
  definition - in this case don't forget to include it at the end of the
  hpp file!)
- Likewise for =PowerOfTwoApprox=. Closely related free functions (such
  as =operator<<=) should be put in the same file(s).
- You may put all =TestDIsplay= functionalities together. Beware: some
  definitions are template and must be in header files, whereas some
  aren't and should therefore be compiler.

To do the exercice, create an /Exercice43/ directory in the TP root (and
include it in your main CMakeLists.txt).

Create the files in this directory, and use the following
CMakeLists.txt:

#+BEGIN_EXAMPLE
  add_library(exercice43_lib
              SHARED
              # Header files are not strictly necessary but may be useful for some CMake generators.
              ${CMAKE_CURRENT_LIST_DIR}/Error.cpp
              ${CMAKE_CURRENT_LIST_DIR}/Error.hpp
              ${CMAKE_CURRENT_LIST_DIR}/PowerOfTwoApprox.hpp
              ${CMAKE_CURRENT_LIST_DIR}/PowerOfTwoApprox.hxx
              ${CMAKE_CURRENT_LIST_DIR}/TestDisplay.cpp
              ${CMAKE_CURRENT_LIST_DIR}/TestDisplay.hpp
              ${CMAKE_CURRENT_LIST_DIR}/TestDisplay.hxx
              ${CMAKE_CURRENT_LIST_DIR}/Tools.hpp
              ${CMAKE_CURRENT_LIST_DIR}/Tools.hxx)


  add_executable(exercice43
                 ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
                 
  target_link_libraries(exercice43 
                        exercice43_lib)
#+END_EXAMPLE
