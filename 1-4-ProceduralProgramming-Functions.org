#+TITLE     An introduction to modern C++
#+AUTHOR    David Chamont (CNRS), Sebastien Gilles (INRIA), Julien Wintz (INRIA)
#+DATE      October 6th, 2020
#+EMAIL     julien.wintz@inria.fr
#+LANGUAGE  ORG|EN.UTF-8

* 1 - Procedural programming - 1.4 - Functions

** Function definition

To be usable in a C++ instruction, a function must be *defined*
beforehand. This definition includes the name and type of the arguments,
the type of the return value and the instruction block that make up the
function.

=void= is a special type to indicate a function doesn't return any
value.

*BEWARE*: Functions can't be defined in blocks, so my trick to
circumvent cling limitation doesn't work. Please execute only once the
cells with function definitions! (restart the kernel if you need to
modify the function content).

#+BEGIN_SRC C++
// Please execute it only once!
#include <iostream>

void PrintDivision(int arg1, int arg2)
{
    if (arg2 == 0)
        std::cerr << "Failure: division by zero!" << std::endl;
    else
    {
        int division ;
        division = arg1/arg2 ;
        std::cout << arg1 << " / " << arg2 << " = " << division << std::endl ;
    }
}
#+END_SRC

#+BEGIN_SRC C++
{
    PrintDivision(12, 3);
    PrintDivision(6, 0);
    PrintDivision(15, 6);
}
#+END_SRC

Functions can't be nested in C++, contrary to some other langages such
as Python:

#+BEGIN_SRC C++
void function_1() // a function might have no arguments
{
    void subfunction() // COMPILATION ERROR!
    {

    }
}
#+END_SRC

To reintroduce hierarchy, *namespaces* can be used (they will be introduced [[../6-InRealEnvironment/5-Namespace.ipynb][a
bit later]]); *lambda functions* introduced later are not limited by the same
rule.

*** Passing arguments by value

In the simple example above, we passed the arguments by value, which is
to say the values passed by the arguments were copied when given to the
function:

#+BEGIN_SRC C++
// Please execute it only once!

#include <iostream>

void increment_and_print(int i)
{
    ++i;
    std::cout << "Inside the function: i = " << i << std::endl;
}

{
    int i = 5; // I could have named it differently - it doesn't matter as the scope is different!

    increment_and_print(i);

    std::cout << "Outside the function: i = " << i << std::endl;
}
#+END_SRC

The =i= in the block body and in the function definition is not the
same: one or the other could have been named differently and the result
would have been the same.

*** Passing arguments by reference

If we intended to modify the value of =i= outside the function, we
should have passed it by reference:

#+BEGIN_SRC C++
// Please execute it only once!

#include <iostream>

void increment_and_print_by_reference(int& i)
{
    ++i;
    std::cout << "Inside the function: i = " << i << std::endl;
}

{
    int i = 5; // I could have named it differently - it doesn't matter as the scope is different!

    increment_and_print_by_reference(i);

    std::cout << "Outside the function: i = " << i << std::endl;
}
#+END_SRC

As in C++ you can't return several values in the return type, passing by
reference is a way to get in output several values (C++ 11 introduced in
the standard library a workaround to get several values in return type
with a so-called =std::tuple=, but the passing by reference remains the
better way to do so in most cases).

#+BEGIN_SRC C++
int compute_division(int arg1, int arg2, int& quotient, int& remainder)
{
    if (arg2 == 0)
        return -1; // error code.

    quotient = arg1 / arg2;
    remainder = arg1 % arg2;
    return 0; // code when everything is alright.
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

{
    int quotient, remainder;

    if (compute_division(5, 3, quotient, remainder) == 0)
        std::cout << "5 / 3 = " << quotient << " with a remainder of " << remainder << '.' << std::endl;

    if (compute_division(5, 0, quotient, remainder) == 0)
        std::cout << "5 / 0 = " << quotient << " with a remainder of " << remainder << '.' << std::endl;
    else
        std::cerr << "Can't divide by 0!" << std::endl;
}
#+END_SRC

*** A bit of wandering: using C-like error codes

The function above gets two outputs: the quotient and the remainder of
the euclidian division. Moreover, this function returns an error code:
by convention this function returns 0 when everything is alright and -1
in case of a zero divider.

Using such an error code is a very common pattern in C, that might as
well be used in C++... The issue is that it requires a lot of discipline
from the user of the function: there are no actual incentive to use the
return value! Just calling =compute_division()= as if it was a void
function is perfectly fine (and yet completely ill-advised). We will see
[[../5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb][later]] the
=exception= mechanism C++ recommends instead of error codes.

Below is an example where things go awry due to the lack of check:

#+BEGIN_SRC C++
// Please execute it only once!
#include <iostream>

void print_division(int arg1, int arg2)
{
    int quotient, remainder;

    compute_division(arg1, arg2, quotient, remainder); // the dev 'forgot' to check the error code.

    std::cout << "Euclidian division of " << arg1 << " by " << arg2 << " yields a quotient of "
              << quotient << " and a remainder of " << remainder << std::endl;
}

print_division(8, 5);
print_division(8, 0); // bug!
#+END_SRC

The developer made two important mistakes: * The return value of
=compute_division= is not checked, so something is printed on screen. *
This something is completely out of control: quotient and remainder
don't get a default value that would help to see if something is askew.
The behaviour is undefined: you have no guarantee on the values the
program will print (currently I see the same values as in the previous
function call, but another compiler/architecture/etc... might yield
another wrong value.

*UPDATE:* I still do not advise to use error codes, but the
[[https://en.cppreference.com/w/cpp/language/attributes/nodiscard][nodiscard]]
attribute introduced in C++ 17 helps your compiler to warn you when the
return value was left unused.

*** Passing arguments by pointers

When the argument of a function is a pointer, each function call results
in the creation of a temporary pointer which is given the address
provided as argument. Then using the =*= operator, you can access the
original variable, not a copy.

Except in the case of interaction with a C library or some /very/
specific cases, I wouldn't advise using passing arguments by pointers:
by reference does the job as neatly and in fact more efficiently
(dereferencing a pointer with =*i= is not completely costless).

#+BEGIN_SRC C++
// Please execute it only once!

#include <iostream>

void increment_and_print_by_pointer(int* i)
{
    ,*i += 1;
    std::cout << "Inside the function: i = " << *i << std::endl;
}

{
    int i = 5; // I could have named it differently - it doesn't matter as the scope is different!

    increment_and_print_by_pointer(&i);

    std::cout << "Outside the function: i = " << i << std::endl;
}
#+END_SRC

** Function with return value

The value to return should come after the keyword =return=.

A C++ function may include several return values in its implementation:

#+BEGIN_SRC C++
#include <iostream>

//! \brief Returns 1 if the value is positive, 0 if it is 0 and -1 if it's negative.
int sign(int a)
{
    if (a > 0)
        return 1;

    if (a == 0)
        return 0;

    return -1;
}

{
    for (int a = -3; a < 4; ++a)
        std::cout << "Sign of " << a << " = " << sign(a) << std::endl;
}
#+END_SRC

** Function overload

*** The easy cases: arguments without ambiguity

It is possible to define several different functions with the exact same
name, provided the type of the argument differ:

#+BEGIN_SRC C++
void f();
#+END_SRC

#+BEGIN_SRC C++
void f(int); // Ok
#+END_SRC

#+BEGIN_SRC C++
void f(double); // Ok
#+END_SRC

#+BEGIN_SRC C++
double f(int, double); // Ok
#+END_SRC

#+BEGIN_SRC C++
#include <string>
std::string f(double, int, char*); // Ok
#+END_SRC

*** [WARNING] Return type doesn't count!

It's not the entire signature of the function that is taken into account
when possible ambiguity is sought by the compiler: the return type isn't
taken into account. So the following cases won't be accepted:

#+BEGIN_SRC C++
void g(int);
#+END_SRC

#+BEGIN_SRC C++
int g(int); // COMPILATION ERROR!
#+END_SRC

If we think about it, it is rather logical: in C++ we are not required
to use the return type of a function (it's not the case in all
languages: Go follows a different path on that topic for instance). The
issue then is that the compiler has no way to know which =g(int)= is
supposed to be called with =g(5)= for instance.

*** Good practice: don't make signature vary only by a reference or a pointer

On the other hand, compiler is completely able to accept signatures that
differs only by a reference or a pointer on one of the argument:

#+BEGIN_SRC C++
#include <iostream>

void h(double a)
{
    std::cout << "h(double) is called with a = " << a << '.' << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

void h(double* a) // Ok
{
    std::cout << "h(double*) is called with a = " << *a << "; a is doubled by the function." << std::endl;

    *a *= 2.;
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

void h(double& a) // Ok... but not advised! (see below)
{
    std::cout << "h(double*) is called with a = " << a << "; a is doubled by the function." << std::endl;
    a *= 2.;
}
#+END_SRC

#+BEGIN_SRC C++
{
    h(5); // Ok
    double x = 1.;
    h(&x); // Ok
}
#+END_SRC

However, there is a possible ambiguity between the pass-by-copy and
pass-by-reference:

#+BEGIN_SRC C++
{
    double x = 1.;
    h(x); // COMPILATION ERROR: should it call h(double) or h(double& )?
}
#+END_SRC

You can lift the ambiguity for the pass-by-value:

#+BEGIN_SRC C++
{
    double x = 1.;
    h(static_cast<double>(x)); // Ok
}
#+END_SRC

But not to my knowledge for the pass-by-reference... So you should
really avoid doing so: if you really need both functions, name them
differently to avoid the ambiguity.

I would even avoid the pointer case: granted, there is no ambiguity for
a computer standpoint, but if you get a developer who is not 100% clear
about the pointer syntax he might end-up calling the wrong function:

#+BEGIN_SRC C++
#include <iostream>

void h2(double a)
{
    std::cout << "h2(double) is called with a = " << a << '.' << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

void h2(double* a)
{
    std::cout << "h2(double*) is called with a = " << *a << "; a is doubled by the function." << std::endl;
    *a *= 2.;
}
#+END_SRC

#+BEGIN_SRC C++
{
    double x = 5.;
    double* ptr = &x;

    h2(x); // call h2(double)
    h2(ptr); // call h2(double*)
    h2(*ptr); // call h2(double)
    h2(&x); // call h2(double*)
}
#+END_SRC

*** Best viable function

In fact, overloading may work even if the match is not perfect: the
*best viable function* is chosen if possible... and some ambiguity may
appear if none matches!

The complete rules are very extensive and may be found
[[https://en.cppreference.com/w/cpp/language/overload_resolution][here]];
as a rule of thumb you should really strive to write overloaded
functions with no easy ambiguity... or not using it at all: sometimes
naming the function differently avoids loads of issues!

#+BEGIN_SRC C++
#include <iostream>

int min(int a, int b)
{
    std::cout << "int version called!" << std::endl;
    return a < b ? a : b;
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

double min(double a, double b)
{
    std::cout << "double version called!" << std::endl;
    return a < b ? a : b;
}
#+END_SRC

#+BEGIN_SRC C++
{
    int i1 { 5 }, i2 { -7 };
    double d1 { 3.14}, d2 { -1.e24};
    float f1 { 3.14f }, f2 { -4.2f};
    short s1 { 5 }, s2 { 7 };

    min(5, 7); // no ambiguity
    min(i1, i2); // no ambiguity
    min(f1, f2); // conversion to closest one
    min(f1, d2); // conversion to closest one
    min(s1, s2); // conversion to closest one
}
#+END_SRC

However, with some other types it doesn't work as well if implicit
converion is dangerous and may loose data:

#+BEGIN_SRC C++
{
    unsigned int i1 { 5 }, i2 { 7 };
    min(i1, i2); // COMPILATION ERROR: no 'obvious' best candidate!
}
#+END_SRC

#+BEGIN_SRC C++
{
    long i1 { 5 }, i2 { 7 };
    min(i1, i2); // COMPILATION ERROR: no 'obvious' best candidate!
}
#+END_SRC

Likewise, if best candidate is not the same for each argument:

#+BEGIN_SRC C++
{
    float f1 { 5.f };
    int i1 { 5 };

    min(f1, i1); // for i1 the 'int'version is better, but for f1 the 'double' is more appropriate...
}
#+END_SRC

*** Advice: use overload only when there is no ambiguity whatsoever

That is when:

- The number of arguments is different between overloads.
- Or their types do not convert implicitly from one to another. For
  instance the following overloads are completely safe to use and the
  interface remains obvious for the end-user:

#+BEGIN_SRC C++
#include <string>
#include <iostream>

std::string GenerateString()
{
    std::cout << "No argument!";
    return "";
}
#+END_SRC

#+BEGIN_SRC C++
std::string GenerateString(char one_character)
{
    std::cout << "One character: ";
    return std::string(1, one_character);
}
#+END_SRC

#+BEGIN_SRC C++
std::string GenerateString(char value1, char value2)
{
    std::cout << "Two characters: ";
    std::string ret(1, value1);
    ret += value2;
    return ret;
}
#+END_SRC

#+BEGIN_SRC C++
std::string GenerateString(const std::string& string)
{
    std::cout << "Std::string: ";
    return string;
}
#+END_SRC

#+BEGIN_SRC C++
std::string GenerateString(const char* string)
{
    std::cout << "Char*: ";
    return std::string(string);
}
#+END_SRC

#+BEGIN_SRC C++
{
    std::cout << GenerateString() << std::endl;
    std::cout << GenerateString('a') << std::endl;
    std::cout << GenerateString('a', 'b') << std::endl;
    std::cout << GenerateString("Hello world!") << std::endl;

    std::string text("Hello!");
    std::cout << GenerateString(text) << std::endl;
}
#+END_SRC

** Optional parameters

It is possible to provide optional parameters in the *declaration* of a
function:

#+BEGIN_SRC C++
// Declaration.
void FunctionWithOptional(double x, double y = 0., double z = 0.);
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

// Definition
void FunctionWithOptional(double x, double y, double z) // notice the absence of default value!
{
    std::cout << '(' << x << ", " << y << ", " << z << ')' << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
// WARNING: Xeus-cling issue!

{
    FunctionWithOptional(3., 5., 6.); // ok
    FunctionWithOptional(3.);         // should be ok, but Xeus-cling issue.
}
#+END_SRC

The reason not to repeat them is rather obvious: if both were accepted
you may modify one of them and forget to modify the others, which would
be a bad design...

There is a way to put it in the same place, that I do not recommend (and
your compiler should warn you most of the time): if you do not declare
the function beforehand, default arguments may be specified at
definition:

#+BEGIN_SRC C++
#include <iostream>

// Definition
void FunctionWithOptional2(double x, double y = 0., double z = 0.)
{
    std::cout << '(' << x << ", " << y << ", " << z << ')' << std::endl;
}
#+END_SRC

#+BEGIN_SRC C++
{
    FunctionWithOptional2(3., 5., 6.); // ok
    FunctionWithOptional2(3.); // ok
}
#+END_SRC

In C and C++, arguments are only *positional*: you do not have a way to
explicitly set an argument with a name for instance.

Therefore:

- Optional arguments must be put together at the end of the function.
- You must think carefully if there are several of them and put the less
  likely to be set manually by the function user at then end. In our
  example above, if you want do call the function with a =x= and a =z=
  you must mandatorily also provide explicitly =y=.

** Lambda functions

C++ 11 introduced a shorthand to define functions called *lambda
functions*.

An example is the best way to introduce them:

#+BEGIN_SRC C++
#include <iostream>

{
    // Locally defined function.
    auto square = [](double x) -> double
    {
        return x * x;
    };

    std::cout << square(5) << std::endl;
}
#+END_SRC

Several notes:

- Use =auto= as its return type; said type is not reproducible (see the
  /square/ and /cube/ example below).
- The symbol =->= that specifies the type of the returned value is
  optional.
- Parameters come after the =[]= in parenthesis with the same syntax as
  ordinary functions.

#+BEGIN_SRC C++
#include <iostream>

{
    // Locally defined function.
    auto square = [](double x)
    {
        return x * x;
    };

    auto cube = [](double x)
    {
        return x * x * x;
    };

    std::cout << "Are the lambda prototypes the same type? "
              << (std::is_same<decltype(square), decltype(cube)>() ? "true" : "false") << std::endl;
}
#+END_SRC

Inside the =[]= you might specify values that are transmitted to the
body of the function; by default nothing is transmitted:

#+BEGIN_SRC C++
#include <iostream>

{
    int a = 5;

    auto a_plus_b = [](int b)
    {
        return a + b;
    };

    std::cout << a_plus_b(3) << std::endl; // COMPILATION ERROR: a is not known inside the lambda body.
}
#+END_SRC

#+BEGIN_SRC C++
#include <iostream>

{
    int a = 5;

    auto a_plus_b = [a](int b) // Notice the `[a]` here!
    {
        return a + b;
    };

    std::cout << a_plus_b(3) << std::endl;
}
#+END_SRC

The values captured in the lambda might be transmitted by reference:

#+BEGIN_SRC C++
#include <iostream>

{
    int a = 5;

    auto add_to_a = [&a](int b) // Notice the `[&a]` here!
    {
        a += b;
    };

    add_to_a(3);

    std::cout << a << std::endl;
}
#+END_SRC

It is possible to capture everything (in the scope where the lambda is
defined) by reference by using =[&]= but it is really ill-advised; don't
do this!

Lambda functions really shines when you want to use them in a very
special context; see below an example using the sort function provided
by the standard library, in which for some reasons we want to sort
integers but in two blocks: first the odd numbers properly ordered and
then the even numbers:

#+BEGIN_SRC C++
// Nice Jupyter Xeus-cling feature that doesn't work for the Conda version of cling in macOS.
?std::sort
#+END_SRC

#+BEGIN_SRC C++
#include <vector>
#include <iostream>
#include <algorithm> // for sort

{
    std::vector<int> list { 3, 5, 2, -4, 8, -17, 99, 15, 125447, 0, -1246 };

    std::cout << "Initial list = ";

    for (int value : list)
        std::cout << value << ' ';

    // My very specific sort operation:
    // Returns true if lhs is odd and rhs isn't or is lhs < rhs.
    auto odd_first = [](auto lhs, auto rhs)
    {
        const bool is_lhs_odd = !(lhs % 2 == 0);
        const bool is_rhs_odd = !(rhs % 2 == 0);

        if (is_lhs_odd != is_rhs_odd)
            return is_lhs_odd;

        return lhs < rhs;
    };

    std::sort(list.begin(), list.end(), odd_first);

    std::cout << std::endl << "Sorted list = ";

    for (int value : list)
        std::cout << value << ' ';
}
#+END_SRC

** Passing a function as a an argument

In some cases, you might want to pass a function as an argument (and
honestly most of the time you should refrain to do so: it may underline
your design is not top notch).

The syntax to do so is a bit ugly and stems directly from C; it relies
upon using a pointer to a function.

The syntax looks like:

=unsigned int (*f) (int, double)=

where:

- =unsigned int= is the return type.
- =int, double= are the type of the parameters of the function given as
  argument.
- =f= is the name of the argument.

It will be clearer in an example:

#+BEGIN_SRC C++
#include <iostream>

void PrintFunctionCall(int (*f) (int, int), int m, int n)
{
    std::cout << "f(" << m << ", " << n << ") = " << f(m, n) << std::endl;
};
#+END_SRC

#+BEGIN_SRC C++
int multiply(int a, int b)
{
    return a * b;
}
#+END_SRC

#+BEGIN_SRC C++
int add(int a, int b)
{
    return a + b;
}
#+END_SRC

#+BEGIN_SRC C++
PrintFunctionCall(multiply, 5, 6);
PrintFunctionCall(add, 5, 6);
#+END_SRC

There are other ways to do this task:

- Using a template parameter. Templates will be reached
  [[../4-Templates/0-main.ipynb][later in this tutorial]], but for me
  it's usually the way to go.
- Using [[../3-Operators/5-Functors.ipynb][functors]]
- Using =std::function=, introduced in C++ 11. However this blog
  explains why it's not a good idea; on top of the arguments given there
  it doesn't seem to respect the prototype closely (a function with
  double instead of int is for instance accepted).

** A very special function: *main*

Any C++ program must include one and only one =main= function. Its
prototype is *int main(int argc, char** argv)* where: * *argc* is the
number of arguments given on the command line. This is at least 1: the
name of the program is one argument. For instance, if your program
creates a /isPrime/ executable that takes an integer as argument, =argc=
will return 2. * *argv* is the list of arguments read on the command
line, given as an array of C-strings. In our /isprime/ example,
*argv[0]* is /isprime/ and *argv[1]* is the integer given.

Please notice the internal mechanics of C/C++ compiler returns these
values; if a user type =isPrime qwerty 20=, the main functions will
return argc = 3. It is up to the writer of the main to ensure the
arguments are correct. If some of these values should be interpreted as
numbers, it is also up to the developer to foresee the conversion from
the C-string to a numerical value.

In the very specific of our Jupyter context, a unique main might be
defined or not in the file: /cling/ performs some magic to generate one
under the hood.

The *main* function may also be defined as *int main()* without
arguments if the program doesn't actually need any.

Sometimes, in old programs you may see *void main()*; this is not
correct and is now refused by most modern compilers.

The return value of the main function is an integer, *EXIT_SUCCESS*
should be returned when the program succeeds and *EXIT_FAILURE* if it
fails. You will often see a numerical value instead of these:
*EXIT_SUCCESS* is just a macro which value is 0. I recommend its use as
you should strive to avoid any magic number in your codes.

We will deal with main functions later when we will work in a true C++
environment.

** =inline= functions

You may also in a function declaration and definition function prepend
the prototype by an =inline=. This indicates the compiler this function
might be *inlined*: this means the content of the function may be copied
directly, thus avoiding a function call and potentially making your code
a tiny bit faster. So for instance if you have a function:

#+BEGIN_SRC C++
inline double square(double x)
{
    return x * x;
}
#+END_SRC

when this function is called somewhere, the compiler may replace
directly the function by the code inside the definition:

#+BEGIN_SRC C++
{
    square(5.); // The compiler might substitute 5. * 5. to the actual function call here
}
#+END_SRC

This behaviour is pretty similar to the often frowned-upon *macros* from
C, but the use of =inline= is not considered a bad practice... provided
you have in mind the way it works:

- You have probably notice the conditional in my statements regarding
  =inline=: the keyword is an /hint/ given to the compiler... that might
  be followed or not.
- On the syntactic side, =inline= must be provided both in the
  declaration =and= the definition.
- =inline= definitions must be provided in header file. You therefore pay the
  price in compilation time whenever you change its implementation.
- Don't bother inlining functions with any complexity whatsoever, so if
  your function includes a loop or is more than few lines long, write a
  normal function instead.

The =square= example was sound: this is typically the kind of functions
that might be inlined.
